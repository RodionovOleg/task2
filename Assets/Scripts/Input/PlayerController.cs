using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions PlayerInputActions;
    private Rigidbody PlayerRigidbody;
    [SerializeField] private float MoveSpeed = 600f;
    private Vector3 startPosition;

    private void Awake()
    {
        PlayerInputActions = new PlayerInputActions();
        PlayerRigidbody = GetComponent<Rigidbody>();
        startPosition = transform.position;

        PlayerInputActions.Kart.ResetPosition.performed += context => ResetPositon();
        PlayerInputActions.Kart.Boost.performed += context => Boost();
        PlayerInputActions.Kart.Slowing.performed += context => Slowing();


    }

    private void OnEnable()
    {
        PlayerInputActions.Enable();
    }

    private void OnDisable()
    {
        PlayerInputActions.Disable();
    }

    private void FixedUpdate()
    {
        Vector2 moveDirection = PlayerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);
    }

    private void Move(Vector2 direction)
    {
        PlayerRigidbody.velocity = new Vector3(direction.x * MoveSpeed * Time.fixedDeltaTime, 0f, direction.y * MoveSpeed * Time.fixedDeltaTime);

    }
   
    private void ResetPositon()
    {
        PlayerRigidbody.MovePosition(startPosition);
        PlayerRigidbody.MoveRotation(Quaternion.identity);
    }

    private void Boost()
    {
        MoveSpeed = MoveSpeed * 2;
    }

    private void Slowing()
    {
        MoveSpeed = MoveSpeed / 2;
    }


}
